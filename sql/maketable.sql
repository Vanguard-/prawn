DROP TABLE IF EXISTS comments
-- DROP TABLE IF EXISTS posts;


CREATE TABLE posts(
  pid VARCHAR(10) PRIMARY KEY NOT NULL,
  pauthor VARCHAR(20),
  title TEXT,
  selftext TEXT,
  created DATE,
  link TEXT,
  n_coms INTEGER,
  subreddit VARCHAR(20) NOT NULL,
  is_removed INTEGER
);

CREATE TABLE comments(
  cid VARCHAR(10) PRIMARY KEY NOT NULL,
  post_id VARCHAR(10) NOT NULL,
  author VARCHAR(20),
  comment TEXT, -- selftext
  created DATE,
  score INTEGER,
  parent_id VARCHAR(10),
  level INTEGER,
  subreddit VARCHAR(20) NOT NULL,
  FOREIGN KEY (post_id) REFERENCES posts(pid),
  FOREIGN KEY (parent_id) REFERENCES comments(cid)
);